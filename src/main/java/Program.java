import com.gurock.testrail.APIClient;
import com.gurock.testrail.APIException;
import org.json.simple.JSONObject;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

/**
 *
 */
public class Program
{
	static
	{
		disableSslVerification();
	}
	
	//отключаем SSL проверки, чтобы можно было работать без установки сертификата
	private static void disableSslVerification()
	{
		try
		{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager()
			{
				public java.security.cert.X509Certificate[] getAcceptedIssuers()
				{
					return null;
				}
				
				public void checkClientTrusted(X509Certificate[] certs, String authType)
				{
				}
				
				public void checkServerTrusted(X509Certificate[] certs, String authType)
				{
				}
			}
			};
			
			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier()
			{
				public boolean verify(String hostname, SSLSession session)
				{
					return true;
				}
			};
			
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		} catch (KeyManagementException e)
		{
			e.printStackTrace();
		}
	}
	
	//обращаемся к функциям Testrail API
	public static void main(String[] args) throws IOException, APIException
	{
		APIClient client = new APIClient("https://10.13.34.85");
		client.setUser("pampushko.o@novaposhta.ua");
		client.setPassword("liBEbc123");
		JSONObject object = (JSONObject) client.sendGet("get_case/2");
		System.out.println(object);
	}
}
